import Command from "./command";
import Balance from "./balance";
import Change from "./change";
import Deposit from "./deposit";
import Withdraw from "./withdraw";
import ChangeBack from "./changeback";

export default class Help extends Command {
    static REGEXP = /^!(?<topic>help)(?:\s+(?<command>\S+))?/;
    static HELP_COMMANDS = [Balance, Change, ChangeBack, Deposit, Withdraw].map(klass => `!${klass.HELP_COMMAND}`).join(' ').trim();

    private command: any;
    constructor(props) {
        super({...props, regExp: Help.REGEXP});
    }

    helpText() {
        return `Available commands: ${Help.HELP_COMMANDS} - type '!${this.topic} <command>' to learn more about the specific command.`;
    }

    findCommand(): Help | Balance | Change | ChangeBack | Deposit | Withdraw {
        const message = this.message.replace(/^!help\s+!?/, '!');
        const findHelp = [Balance, Change, ChangeBack, Deposit, Withdraw].find(klass => message.match(klass.REGEXP));
        if (findHelp) {
            return new findHelp({
                message: message,
                tags: this.tags,
            });
        }
        return this;
    }

    getHelpText(): string {
        const command = this.findCommand();
        let helpText = command.helpText();
        if (command !== this) {
            helpText = `@${this.displayName} !${command.topic} ${helpText}`;
        }
        return helpText;
    }

    handleMessage() {
        if (!this.match) return;
        this.say(this.getHelpText());
    }
}
