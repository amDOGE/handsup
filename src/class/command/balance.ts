import Command from "./command";

export default class Balance extends Command {
    static REGEXP = /^!(?<topic>ba(?:lance|nk))/;
    static HELP_COMMAND = 'balance';

    constructor(props) {
        super({...props, regExp: Balance.REGEXP});
    }

    helpText(): string {
        return `returns your balance in coin available to your account.`;
    }

    async handleMessage() {
        if (!this.match) return;
        const cryptoBalance = await this.clients.crypto.getBalance(this.userId);
        const streamElementsBalance = await this.clients.streamElements.getUserPoints(this.tags.username);
        // await this.twitchClient.whisper(tags.username, `You currently have ${balance} Koin`);
        await this.say(`@${this.displayName} you currently have ${streamElementsBalance.points} Kapp coin and ${cryptoBalance} pandacoin.`);
    }
}
