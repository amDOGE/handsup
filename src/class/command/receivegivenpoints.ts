import Command from "./command";
import BigNumber from "bignumber.js";

export default class ReceiveGivenPoints extends Command {
    // itsnoface gave 10 points to streamelements PogChamp
    // this format will differ per channel, and might need to be adjusted
    // probably not needed but i'm stripping out any @ and , in here
    static REGEXP = /^@?(?<username>\S+),? gave (?<amount>\d+)(?<kilo>k)? points to @?(?<receiver>\S+),? (?<emote>\S+)$/;

    username: string;
    amount: number;
    kilo: string;
    receiver: string;
    emote: string;
    constructor(props) {
        super({...props, regExp: ReceiveGivenPoints.REGEXP});
        if (this.kilo === 'k') {
            this.amount = new BigNumber(this.amount).multipliedBy(1000).toNumber();
            this.kilo = undefined;
        }
    }

    async handleMessage() {
        if (!this.match) return;
        if (this.userId !== "100135110" || !this.tags.mod || this.tags.username !== "streamelements") return; // 100135110 is SE
        if (!this.username || !this.amount || !this.receiver) return;
        if (this.receiver !== global.main.identity.clients.twitch.username) return;
        if (this.emote !== 'PogChamp') return;
        const lastMessage = global.main.lastGivePointsByUsername.get(this.username);
        const userId = lastMessage.tags['user-id'];
        if (!userId) return;
        const balance = await this.clients.crypto.getBalance("");
        const amountBigNum = new BigNumber(this.amount);
        const balanceBigNum = new BigNumber(balance);
        if (!balance || balanceBigNum.isNaN() || balanceBigNum.lte(0)) return;
        if (amountBigNum.isNaN() || amountBigNum.gt(balanceBigNum.minus(100).toFixed(0)) || amountBigNum.lt(1)) {
            this.say(`!givepoints ${this.username} ${amountBigNum} sorry, but you cannot convert that amount Sadge (max: ${balanceBigNum.minus(100).toFixed(0)})`);
            return;
        }
        global.main.lastGivePointsByUsername.delete(this.username);
        const res = await this.clients.crypto.move("", userId.toString(), amountBigNum.toNumber());
        this.say(`${res}`);
    }
}
