import Command from "./command";
import BigNumber from "bignumber.js";
import ChangeBack from "./changeback";

export default class Change extends Command {
    static REGEXP = /^!(?<topic>change)(?!back)(?:\s+(?<amount>[\d.]+)(?<kilo>k)?)?/;
    static HELP_COMMAND = 'change';

    amount: any;
    kilo: any;
    constructor(props) {
        super({...props, regExp: Change.REGEXP});
        if (this.kilo === 'k') {
            this.amount = new BigNumber(this.amount).multipliedBy(1000).toNumber();
            this.kilo = undefined;
        }
    }

    helpText(): string {
        return `converts the specified amount of pandacoins into Kapp coins. ${this.usageText()}`;
    }

    usageText(): string {
        return `usage: !${this.topic} <amount> - use '!${ChangeBack.HELP_COMMAND} <amount>' to convert your Kapp coins to pandacoins.`;
    }

    async handleMessage() {
        if (!this.match) return;
        if (!this.amount) {
            this.say(`@${this.displayName} ${this.usageText()}`);
            return;
        }
        const balance = await this.clients.crypto.getBalance(this.userId);
        const balanceBigNum = new BigNumber(balance);
        const amountBigNum = new BigNumber(this.amount);
        if (!balance || balanceBigNum.isNaN() || balanceBigNum.lte(0)) return;
        if (!amountBigNum.isInteger() || amountBigNum.lte(0) || amountBigNum.gt(balanceBigNum)) { // isInteger implies isNaN
            this.say(`@${this.displayName} rcdKrappa`);
            return;
        }
        const res = await this.clients.crypto.move(this.userId, "", amountBigNum.toNumber());
        if (res) {
            await this.clients.streamElements.addUserPoints(this.tags.username, amountBigNum.toNumber());
            this.say(`@${this.displayName} +${amountBigNum} Kapp , ${amountBigNum.negated()} crypto`);
        }
    }
}
