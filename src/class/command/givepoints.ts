import Command from "./command";
import BigNumber from "bignumber.js";

export default class GivePoints extends Command {
    static REGEXP = /^!(?<topic>give(?:points)?)(?:\s+@?(?<username>\S+),?\s+(?<amount>\d+)(?<kilo>k)?)?/;

    username: any;
    amount: any;
    kilo: any;
    constructor(props) {
        super({...props, regExp: GivePoints.REGEXP});
        if (this.kilo === 'k') {
            this.amount = new BigNumber(this.amount).multipliedBy(1000);
            this.kilo = undefined;
        }
    }

    async handleMessage() {
        if (!this.match) return;
        // we don't own this command, we just listen to it happening
        let {username, amount} = this;
        if (!username || !amount) return;
        if (username !== global.main.identity.clients.twitch.username) return;
        if (!parseInt(amount)) return;
        global.main.lastGivePointsByUsername.set(this.tags.username, {
            channel: this.channel,
            tags: this.tags,
            message: this.message,
        });
    }
}
