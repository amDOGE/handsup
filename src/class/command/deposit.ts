import Command from "./command";

export default class Deposit extends Command {
    static REGEXP = /^!(?<topic>g(?:w|etwallet)|deposit)/;
    static HELP_COMMAND = 'deposit';

    constructor(props) {
        super({...props, regExp: Deposit.REGEXP});
    }

    helpText(): string {
        return `returns an unused address which can be used for deposit.`;
    }

    async handleMessage() {
        if (!this.match) return;
        const address = await this.clients.crypto.getAccountAddress(this.userId);
        this.say(`@${this.displayName} ${address}`);
    }
}
