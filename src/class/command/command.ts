export default class Command {
    clients: any;
    channel: string;
    tags: any;
    message: string;
    private readonly regExp: any;
    public readonly topic: string;
    displayName: string;
    userId: string;
    match: RegExpMatchArray;
    constructor(props) {
        this.clients = props.clients;
        this.channel = props.channel;
        this.tags = props.tags;
        this.message = props.message;
        this.regExp = props.regExp;
        this.displayName = this.tags['display-name'];
        this.userId = this.tags['user-id'];
        this.match =  this.message.match(this.regExp);
        if (this.match?.groups) { // add matches to object, might be debatable
            for (const [key, value] of Object.entries(this.match.groups)) {
                this[key] = value;
            }
        }
    }

    say(message) {
        return this.clients.twitch.say(this.channel, message);
    }
}
