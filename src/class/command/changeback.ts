import Command from "./command";
import BigNumber from "bignumber.js";

export default class ChangeBack extends Command {
    static REGEXP = /^!(?<topic>changeback)(?:\s+(?<amount>[\d.]+)(?<kilo>k)?)?/;
    static HELP_COMMAND = 'changeback';

    amount: any;
    kilo: any;
    constructor(props) {
        super({...props, regExp: ChangeBack.REGEXP});
        if (this.kilo === 'k') {
            this.amount = new BigNumber(this.amount).multipliedBy(1000).toNumber();
            this.kilo = undefined;
        }
    }

    helpText(): string {
        return `converts the specified amount of Kapp coins into pandacoins. ${this.usageText()}`;
    }

    usageText(): string {
        return `usage: !${this.topic} <amount>`;
    }

    async handleMessage() {
        if (!this.match) return;
        if (!this.amount || !this.userId) return;
        const balance = await this.clients.crypto.getBalance("");
        const streamElementsBalance = (await this.clients.streamElements.getUserPoints(this.tags.username))?.points;
        const balanceBigNum = new BigNumber(balance);
        const streamElementsBalanceBigNum = new BigNumber(streamElementsBalance);
        const amountBigNum = new BigNumber(this.amount);
        if (!balance || balanceBigNum.isNaN() || balanceBigNum.lte(0)) return;
        if (!streamElementsBalanceBigNum || streamElementsBalanceBigNum.isNaN() || streamElementsBalanceBigNum.lte(0)  || amountBigNum.gt(streamElementsBalanceBigNum)) {
            this.say(`@${this.displayName} rcdKrappa`);
            return;
        }
        if (amountBigNum.isNaN() || amountBigNum.gt(balanceBigNum.minus(100).toFixed(0)) || amountBigNum.lt(1)) {
            this.say(`@${this.displayName} sorry, but you cannot convert that amount Sadge (max: ${balanceBigNum.minus(100).toFixed(0)})`);
            return;
        }
        const res = await this.clients.crypto.move("", this.userId, amountBigNum.toNumber());
        if (res) {
            await this.clients.streamElements.removeUserPoints(this.tags.username, amountBigNum.toNumber());
            this.say(`@${this.displayName} ${amountBigNum.negated()} Kapp , +${amountBigNum} crypto`);
        }
    }
}
