import Command from "./command";
import BigNumber from "bignumber.js";
const bitcoin = require('bitcoinjs-lib');

export default class Withdraw extends Command {
    static REGEXP = /^!(?<topic>w(?:d|ithdraw))(?:\s+(?<address>[APnm]{1}[a-km-zA-HJ-NP-Z1-9]{26,33}|(?:pn|tpn|pnrt)1[a-z0-9]{39,59})\s+(?<amount>[\d.]+)(?<kilo>k)?)?/;
    static HELP_COMMAND = 'withdraw';

    address: any;
    amount: any;
    kilo: any;
    addressIsValid: boolean;
    buffer: number;
    constructor(props) {
        super({...props, regExp: Withdraw.REGEXP});
        this.addressIsValid = this.isAddressValid();
        this.buffer = 100;
        if (this.kilo === 'k') {
            this.amount = new BigNumber(this.amount).multipliedBy(1000).toNumber();
            this.kilo = undefined;
        }
    }

    helpText(): string {
        return `allows you to withdraw coins to your wallet. ${this.usageText()}`;
    }

    usageText(): string {
        return `usage: !${this.topic} <address> <amount>`;
    }

    isAddressValid(address=this.address): boolean {
        try {
            bitcoin.address.toOutputScript(address, global.NETWORK);
            return true;
        } catch (_) {
            return false;
        }
    }

    async handleMessage() {
        if (!this.match) return;
        let {address, addressIsValid, amount} = this;
        const displayName = this.displayName;
        if (!address || !amount) {
            this.say(`@${displayName} ${this.usageText()}`);
            return;
        }
        if (address && !addressIsValid) {
            this.say(`@${displayName} the address you supplied is invalid.`);
            return;
        }
        amount = parseFloat(amount);
        const balance = await this.clients.crypto.getBalance(this.userId);
        let amountBigNum = new BigNumber(amount);
        const balanceBigNum = new BigNumber(balance);
        if (!balance || balanceBigNum.isNaN() || balanceBigNum.lte(0)) {
            this.say(`@${displayName} you have insufficient funds (your balance: ${balanceBigNum}).`);
            return;
        }
        if (!amount || amountBigNum.isNaN()) {
            this.say(`@${displayName} the amount (parsed: ${amountBigNum}) you supplied is invalid.`);
            return;
        }
        if (amountBigNum.gt(balanceBigNum.minus(this.buffer))) {
            this.say(`@${displayName} not enough balance left to send (balance: ${balanceBigNum}, buffer: ${this.buffer}, after fee and buffer: ${balanceBigNum.minus(this.buffer).minus(amountBigNum)}).`);
            return;
        }
        if (amountBigNum.lt(1)) {
            this.say(`@${displayName} you must send at least 1 coin.`);
            return;
        }
        await this.say(`ppMitosis uid: ${this.userId}, address: ${address}, amount: ${amountBigNum}`);
        const txId = await this.clients.crypto.sendFrom(this.userId, address, amountBigNum.toNumber()); // todo up the confirmation count needed
        this.say(`@${displayName} here you go, sir: ${txId}`);
    }
}
