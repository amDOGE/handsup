import Help from "./class/command/help";
import Balance from "./class/command/balance";
import Change from "./class/command/change";
import Withdraw from "./class/command/withdraw";
import Deposit from "./class/command/deposit";
import ChangeBack from "./class/command/changeback";
const bitcoin = require('bitcoinjs-lib');

global.NETWORK = bitcoin.networks.testnet;

const fs = require('fs').promises;
const tmi = require('tmi.js');
const PNDClient = require('bitcoin-core');
const StreamElements = require('node-streamelements');

class Main {
    clients: {
        crypto: any,
        twitch: any,
        streamElements: any,
    }
    channel: string;
    lastGivePointsByUsername: Map<string, any>;
    identity: {
        clients: {
            crypto: {
                host: string,
                port: number,
                network: "regtest" | "testnet" | "mainnet",
                username: string,
                password: string,
            },
            twitch: {
                username: string,
                password: string,
            },
            streamElements: {
                token: string,
                accountId: string,
            }
        }
    };
    constructor(props) {
        this.channel = props.channel;
        this.clients = {
            crypto: undefined,
            twitch: undefined,
            streamElements: undefined,
        };
        this.lastGivePointsByUsername = new Map();
        this.init();
    }

    async init () {
        const data = await fs.readFile(".env.json");
        this.identity = JSON.parse(data);
        this.addPNDClient();
        this.addSEClient();
        this.addTwitchClient();
        this.clients.twitch.on('message', this.handleMessage.bind(this));
    }

    addPNDClient () {
        this.clients.crypto = new PNDClient({
            version: '0.16.3',
            ...this.identity.clients.crypto,
        });
    }

    addTwitchClient () {
        this.clients.twitch = new tmi.Client({
            connection: {
                secure: true,
                reconnect: true
            },
            identity: this.identity.clients.twitch,
            channels: [this.channel]
        });
        this.clients.twitch.connect()
            .then(() => console.log(`Connected to ${this.channel}`));
    }

    addSEClient () {
        this.clients.streamElements = new StreamElements(this.identity.clients.streamElements);
    }

    handleMessage (channel, tags, message, self) {
        if (self) return;
        console.log(channel, tags, message, self);
        [
            Balance,
            Change,
            ChangeBack,
            Deposit,
            Help,
            Withdraw,
        ].forEach(klass => {
            if (message.match(klass.REGEXP))
                new klass({clients: this.clients, channel, message, tags}).handleMessage();
        });
    }
}

global.main = new Main({channel: 'itsnoface'});
//setInterval(() => {}, 1 << 30);
