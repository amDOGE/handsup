FROM node:16-alpine as build
RUN apk add git
WORKDIR /build
COPY package*.json /build/
RUN npm install

FROM node:16-alpine
WORKDIR /app
COPY --from=build /build/node_modules /app/node_modules
COPY . .
RUN npx tsc
ENTRYPOINT ["npm"]
CMD ["start"]
